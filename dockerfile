FROM python
MAINTAINER Pedro Vidal
RUN git clone https://pedrovid@bitbucket.org/pedrovid/dockerizedapp.git
ENV HOME /root
ENTRYPOINT cd dockerizedapp
CMD python program.py 
